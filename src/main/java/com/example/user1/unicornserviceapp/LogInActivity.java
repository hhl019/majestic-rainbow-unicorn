package com.example.user1.unicornserviceapp;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.parse.Parse;
import com.parse.ParseUser;


public class LogInActivity extends ActionBarActivity {

    protected EditText lgUsername;
    protected EditText lgPassword;
    protected Button lgLogin;
    protected Button lgCreateAccount;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "hpltUpBi1pSvjmNTNvYYOzgFLh8FoC3ouBbo88NG", "hcYh4k0ZTzjrohsZ3E2VmjckgWSWxb1gpVsVF5CB");

        setContentView(R.layout.login_activity);

        //initialize
        lgUsername = (EditText)findViewById(R.id.loginName);
        lgPassword = (EditText)findViewById(R.id.loginPassword);
        lgLogin = (Button)findViewById(R.id.loginButton);
        lgCreateAccount = (Button)findViewById(R.id.loginCreateAccount);


        //listen to when the log in button is clicked
        lgLogin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 //get the inputs from edit text and convert to string
                String userName = lgUsername.getText().toString().trim();
                String passWord = lgPassword.getText().toString().trim();
            }
        });

        //log in the user using parse sdk









    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
